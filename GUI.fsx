// Prelude
open System 
open System.Net 
open System.Threading 
open System.Windows.Forms 
open System.Drawing 
open System.Text.RegularExpressions

#r "EventHandling.dll"
open EventHandling
#load "model.fsx"



// The window part
let window =
  new Form(Text="NIM", Size=Size(525,225))

let label n x =
            let temp = new Label()
            do temp.Size <-new System.Drawing.Size(100,100)
            do temp.Text <- sprintf "%i" n
            do temp.AutoSize <- true
            do temp.Location <- new Drawing.Point(x,40) 
            temp
let statusBox =
  new Label(Location=Point(150,5),Size=Size(200,25))
statusBox.Text <- "Welcome!"
let resetButton =
  new Button(Location=Point(50,120),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="RESET")

let moveButton =
  new Button(Location=Point(200,120),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="MOVE")

let randomNumbers n =
    let rnd = System.Random()
    Array.init n (fun _ -> rnd.Next(1,10))

let generateXes n =
    [|for i in 0 .. (n-1) -> 50 + i * 50|]

let generateBoxes n = 
    generateXes n
    |> Array.map (fun x -> new TextBox(Location=Point(x,65),Size=Size(25,25)))
     
let generateLabels values n = 
    generateXes n
    |> Array.map2 (fun v x -> label v x) values
    
let setBoxValues nimBoxes values = 
    Array.map2 (fun (b : TextBox) v -> b.Text <- v.ToString()) nimBoxes values

let setLabelValues labels values = 
    Array.map2 (fun (l : Label) v -> l.Text <- v.ToString()) labels values

let boxes = 5
let nums = randomNumbers boxes
let theBoxes = generateBoxes boxes
let labels = generateLabels nums boxes
let initializeLabels =
    Array.map (fun l ->  window.Controls.Add l) labels


let disable bs = 
    for b in [resetButton;moveButton] do 
        b.Enabled  <- true
    for (b:Button) in bs do 
        b.Enabled  <- false

// An enumeration of the possible events 
type Message =
  | Reset | Move | Web of string | Error | Cancelled 

//exception UnexpectedMessage
let getBoxValue (box : TextBox) = try int box.Text
                                  with | :? System.FormatException -> -1

                                  
let getLabelValues labels = Array.collect (fun (label : Label) -> [|int label.Text|]) labels

let getBoxValues boxes = Array.collect (fun box -> [|getBoxValue box|]) boxes

let updateLabels labels values = Array.map2 (fun (label : Label) value -> label.Text <- value.ToString()) labels values

let updateTextBoxes boxes values = Array.map2 (fun (box : TextBox) value -> box.Text <- value.ToString()) boxes values

// The dialogue automaton 
let ev = AsyncEventQueue()
let rec ready() = 
  async {//urlBox.Text <- "http://"
         //ansBox.Text <- ""
         

         //disable [cancelButton]
         let! msg = ev.Receive()
         match msg with
         | Reset    -> return! reset()
         | Move     -> return! move()
         | _         -> failwith("ready: unexpected message")}
 
and reset() =
    async {
        let nums = randomNumbers boxes
        let changeLabels = setLabelValues labels nums
        let changeBoxes = setBoxValues theBoxes nums
        ignore (Array.map (fun (b : TextBox) -> b.Enabled <- true) theBoxes)
        statusBox.Text <- "Make a move"
        moveButton.Enabled <- false
        return! ready()
    }
and move() =
    async {
        printfn "%A" (getBoxValues theBoxes)

        ignore (updateLabels labels [|1;2;3;4;5|])
        ignore (updateTextBoxes theBoxes [|1;2;3;4;5|])
        return! ready()
    }
//and loading(url) =
//  async {//ansBox.Text <- "Downloading"
//         use ts = new CancellationTokenSource()
//
//          // start the load
//         Async.StartWithContinuations
//             (async { let webCl = new WebClient()
//                      let! html = webCl.AsyncDownloadString(Uri url)
//                      return html },
//              (fun html -> ev.Post (Web html)),
//              (fun _ -> ev.Post Error),
//              (fun _ -> ev.Post Cancelled),
//              ts.Token)
//
//         disable [resetButton; moveButton]   
//         let! msg = ev.Receive()
//         match msg with
//         | Web html ->
//             let ans = "Length = " + String.Format("{0:D}",html.Length)
//             return! finished(ans)
//         | Error   -> return! finished("Error")
//         | Move  -> ts.Cancel()
//                      return! cancelling()
//         | _       -> failwith("loading: unexpected message")}
//
//and cancelling() =
//  async {//ansBox.Text <- "Cancelling"
//         
//         disable [resetButton; moveButton]
//         let! msg = ev.Receive()
//         match msg with
//         | Cancelled | Error | Web  _ ->
//                   return! finished("Cancelled")
//         | _    ->  failwith("cancelling: unexpected message")}
//
//and finished(s) =
//  async {//ansBox.Text <- s
//         
//         disable [resetButton]
//         let! msg = ev.Receive()
//         match msg with
//         | Move -> return! ready()
//         | _     ->  failwith("finished: unexpected message")}

// Initialization
window.Controls.Add resetButton
window.Controls.Add moveButton
moveButton.Enabled <- false
window.Controls.Add statusBox
//window.Controls.Add cancelButton
resetButton.Click.Add (fun _ -> ev.Post Reset)
moveButton.Click.Add (fun _ -> ev.Post Move)
//cancelButton.Click.Add (fun _ -> ev.Post Cancel)
let numRegex = "[0-9]+" 
//let initializeNimboxes =
//    Array.map (fun (box : TextBox) ->   box.TextChanged.Add (fun _ -> let matches = Regex.Match(box.Text, numRegex) in
//                                                                      if matches.Success then () else statusBox.Text <- "nej!")
//                                        window.Controls.Add box) theBoxes
let initializeNimboxes =
    Array.map (fun (box : TextBox) ->  window.Controls.Add box) theBoxes
let changeBoxes = setBoxValues theBoxes nums
let disableNotUsedBoxes index = Array.mapi (fun i elem -> if i <> index then theBoxes.[i].Enabled <- false) theBoxes

let textChangedEvent box _  =  let index = Array.findIndex (fun b -> b = box) theBoxes
                               let nums = getLabelValues labels
                               if box.Text <> "" && getBoxValue box = nums.[index]                                                                                                                        
                               then ignore (Array.map (fun (b : TextBox) -> b.Enabled <- true) theBoxes)
                               else if getBoxValue box < nums.[index] && getBoxValue box > -1
                                    then ignore (disableNotUsedBoxes index);statusBox.Text <- "Press MOVE"; moveButton.Enabled <- true
                                    else statusBox.Text <- "Input invalid"; moveButton.Enabled <- false
let keypressEvent (ev : KeyPressEventArgs) = if not (Char.IsNumber ev.KeyChar || Char.IsControl ev.KeyChar)  then ev.Handled <- true
let addTextChanged =
    Array.map (fun (box : TextBox) ->   box.TextChanged.Add (textChangedEvent box)
                                        box.KeyPress.Add keypressEvent) theBoxes



// Start
Async.StartImmediate (ready())
window.Show() // on Mono-platforms: replace 'window.Show()' with 'Application.Run(window)'
