﻿open System.IO

let localpath = @"C:\DTU\02257Func\02257-project1\project3\"

type Tree<'a> = Node of 'a * Tree<'a> list
type Extent = float * float list

let movetree (Node((label, x), subtrees), x' : float) = Node((label, x + x'), subtrees)

let moveextent (e,x:float) = List.map (fun (p,q) -> (p+x, q+x)) e


let rec merge = function
    | ([], qs)               -> qs
    | (ps, [])               -> ps
    | ((p,_)::ps, (_,q)::qs) -> (p,q) :: merge (ps,qs)

let mergelist es = List.foldBack (fun e acc -> (merge (e,acc))) es []

let rec fit = function
    | ((_,p)::ps, ((q,_)::qs)) -> max (fit (ps,qs)) (p - q + 1.0)
    | _                        -> 0.0

let fitlistl es = let rec fitlistl' acc = function
                      | [] -> []
                      | e::es -> let x = fit (acc,e)
                                 x :: fitlistl' (merge (acc, moveextent (e,x))) es
                  fitlistl' [] es

let fitlistr es = let flipextent e = List.map (fun (p,q) -> (-q,-p)) e
                  List.rev (List.map (~-) (fitlistl (List.map flipextent (List.rev es))))
                  

let fitlist es = 
    let mean (x,y) = (x+y)/2.0
    List.map mean (List.zip (fitlistl es) (fitlistr es))

let design tree = let rec design' (Node(label, subtrees)) = 
                       let (trees, extents) = List.unzip (List.map design' subtrees)
                       let positions = fitlist extents
                       let ptrees = List.map movetree (List.zip trees positions)
                       let pextents = List.map moveextent (List.zip extents positions)
                       let resultextent = (0.0, 0.0) :: mergelist pextents
                       let resulttree = Node((label, 0.0), ptrees)
                       printfn "%A" resultextent
                       (resulttree, resultextent)

                  fst (design' tree)



let openfile filename = new System.IO.StreamWriter(localpath + filename + ".ps", true)

let write (sw : System.IO.StreamWriter) (s : string) = sw.Write(s)

let closefile (sw : System.IO.StreamWriter) = sw.Close()                  




let moveto x y = x.ToString() + " " + y.ToString() + " moveto\n"
let lineto x y = x.ToString() + " " + y.ToString() + " lineto\n"
let makeLabel s = "(" + s.ToString() + ") dup stringwidth pop 2 div neg 0 rmoveto show\n"
let stroke = "stroke\n"
let header = "%!\n" +
             "<</PageSize[1400 1000]/ImagingBBox null>> setpagedevice\n" +
             "1 1 scale\n" +
             "700 999 translate\n" +
             "newpath\n" +
             "/Times-Roman findfont 10 scalefont setfont\n"
let showpage = "showpage"

let writeLabel height hori label = moveto (hori*40.0) (height-10.0) +
                                   makeLabel label

let writeNode height hori label bounds = writeLabel height hori label +
                                         moveto (hori*40.0) (height-20.0) + 
                                         lineto (hori*40.0) (height-40.0) +
                                         moveto ((fst bounds) * 40.0) (height-40.0) +
                                         lineto ((snd bounds) * 40.0) (height-40.0)
                                  


let writeChild height hori = moveto (hori*40.0) (height-40.0) + 
                             lineto (hori*40.0) (height-80.0)

let extractHori (Node((_,hori), _)) = hori                             
    
let rec toPS (sw : System.IO.StreamWriter) my_height my_parent_hori =
    function
    | Node((label,my_relative_hori),[]) -> write sw (writeLabel my_height (my_parent_hori + my_relative_hori) label)
    | Node((label,my_relative_hori),children) -> let bounds = (my_parent_hori + my_relative_hori + extractHori (List.head children), my_parent_hori + my_relative_hori + extractHori (List.last children))
                                                 write sw (writeNode my_height (my_parent_hori + my_relative_hori) label bounds)
                                                 List.iter (fun (Node((_,child_hori),_)) -> write sw (writeChild my_height (my_parent_hori + my_relative_hori + child_hori))) children
                                                 write sw stroke
                                                 List.iter (toPS sw (my_height - 80.0) (my_parent_hori + my_relative_hori)) children

let run tree =
    let sw = openfile "test"
    write sw header
    toPS sw 0.0 0.0 tree
    write sw showpage
    closefile sw

let tree = design (Node(2.0, [Node(3.0,[Node(3.0,[])]);Node(5.0,[Node(2.0, [Node(3.0,[Node(3.0,[])]);Node(5.0,[]);Node(6.0,[Node(2.0, [Node(3.0,[Node(3.0,[])]);Node(5.0,[]);Node(6.0,[])])])])]);Node(6.0,[Node(2.0, [Node(3.0,[Node(3.0,[])]);Node(5.0,[]);Node(6.0,[])])])]));;

run tree





