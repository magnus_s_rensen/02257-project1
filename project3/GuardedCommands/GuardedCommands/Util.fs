﻿namespace GuardedCommands.Util


open System.IO
open System.Text
open Microsoft.FSharp.Text.Lexing

open GuardedCommands.Frontend.AST
open Parser
open Lexer

open Postscripter
open PostscripterConcat

open Machine
open VirtualMachine

module ParserUtil = 

   let parseString (text:string) =
      let lexbuf = LexBuffer<_>.FromBytes(Encoding.UTF8.GetBytes(text))
      try
           Main Lexer.tokenize lexbuf
      with e ->
           let pos = lexbuf.EndPos
           printfn "Error near line %d, character %d\n" pos.Line pos.Column
           failwith "parser termination"


// Parse a file. (A statement is parsed) 
   let parseFromFile filename =
      if File.Exists(filename)    
      then parseString(File.ReadAllText(filename))
      else invalidArg "ParserUtil" "File not found"

open ParserUtil



module PostscripterUtil =

   let printPS filename = let prog = parseFromFile filename
                          Postscripter.makePostscript (Postscripter.design (Postscripter.programToTree prog)) (filename.Substring(0, filename.Length-3))
                           
   let printPStimed filename = let prog = parseFromFile filename
                               printfn "Program size: %d" (Postscripter.programCount prog)
                               Postscripter.duration (fun () -> Postscripter.makePostscript (Postscripter.design (Postscripter.programToTree prog)) 
                                                                                             (filename.Substring(0, filename.Length-3)))

   let printPSgenericTimed depth = printfn "Generic tree depth: %d" depth
                                   let tree = Postscripter.makeTree depth
                                   Postscripter.duration (fun () -> Postscripter.makePostscript (Postscripter.design tree) ("plus" + depth.ToString()))

   let printPSconcatTimed filename = let prog = parseFromFile filename
                                     printfn "Program size: %d" (PostscripterConcat.programCount prog)
                                     PostscripterConcat.duration (fun () -> PostscripterConcat.makePostscript (PostscripterConcat.design (PostscripterConcat.programToTree prog)) 
                                                                                                        (filename.Substring(0, filename.Length-3)))

   let printPSgenericConcatTimed depth = printfn "Generic tree depth: %d" depth
                                         let tree = PostscripterConcat.makeTree depth
                                         PostscripterConcat.duration (fun () -> PostscripterConcat.makePostscript (PostscripterConcat.design tree) ("concat" + depth.ToString()))