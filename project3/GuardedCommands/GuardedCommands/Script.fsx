﻿//Revise this path
System.IO.Directory.SetCurrentDirectory @"C:\DTU\02257Func\02257-project1\project3\GuardedCommands\GuardedCommands";;

#r @"bin\Debug\FSharp.PowerPack.dll";;
#r @"bin\Debug\Machine.dll";
#r @"bin\Debug\VirtualMachine.dll";

#load "AST.fs"
#load "Parser.fs"
#load "Lexer.fs"
#load "Postscripter.fs"
#load "PostscripterConcat.fs"
#load "Util.fs"


open GuardedCommands.Util
open GuardedCommands.Frontend.AST

open Postscripter
open PostscripterConcat

open ParserUtil
open PostscripterUtil

open Machine
open VirtualMachine

//Make postscript files for project-2 programs
List.iter printPS ["Ex0.gc"; "Ex1.gc"; "Ex2.gc"; "Ex3.gc"; "Ex4.gc"; "Ex5.gc"; "Ex6.gc"; "Ex7.gc"; "Skip.gc"; "fact.gc"; "factCBV.gc"; "factRec.gc"; "A0.gc";
                         "A1.gc"; "A2.gc"; "A3.gc"; "A4.gc"; "Swap.gc"; "QuickSortV1.gc"; "par1.gc"; "factImpPTyp.gc"; "QuickSortV2.gc"; "par2.gc"]

//Make postscript files for project-2 programs while printing program size and execution timed
//printfn "----------- Using + concatenation ------------"
//List.iter printPStimed ["Ex0.gc"; "Ex1.gc"; "Ex2.gc"; "Ex3.gc"; "Ex4.gc"; "Ex5.gc"; "Ex6.gc"; "Ex7.gc"; "Skip.gc"; "fact.gc"; "factCBV.gc"; "factRec.gc"; "A0.gc";
//                         "A1.gc"; "A2.gc"; "A3.gc"; "A4.gc"; "Swap.gc"; "QuickSortV1.gc"; "par1.gc"; "factImpPTyp.gc"; "QuickSortV2.gc"; "par2.gc"]
//printfn "----------- Using String.concat ------------"
//List.iter printPSconcatTimed ["Ex0.gc"; "Ex1.gc"; "Ex2.gc"; "Ex3.gc"; "Ex4.gc"; "Ex5.gc"; "Ex6.gc"; "Ex7.gc"; "Skip.gc"; "fact.gc"; "factCBV.gc"; "factRec.gc"; "A0.gc";
//                         "A1.gc"; "A2.gc"; "A3.gc"; "A4.gc"; "Swap.gc"; "QuickSortV1.gc"; "par1.gc"; "factImpPTyp.gc"; "QuickSortV2.gc"; "par2.gc"]
//                         
//
////Make postscript files for generic (binary) trees.
////Note: The following will use > 400 mb of disc space
//printfn "----------- Using + concatenation ------------"
//let genericPlus = for i = 1 to 20 do
//                      printPSgenericTimed i
//
//printfn "----------- Using String.concat ------------"
//let genericConcat = for i = 1 to 20 do
//                        printPSgenericConcatTimed i
