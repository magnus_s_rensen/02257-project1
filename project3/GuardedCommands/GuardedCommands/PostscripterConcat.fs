﻿module PostscripterConcat

//Uses String.concat

open System
open GuardedCommands.Frontend.AST

type Tree<'a> = Node of 'a * Tree<'a> list
type Extent = float * float list

let movetree (Node((label, x), subtrees), x' : float) = Node((label, x + x'), subtrees)

let moveextent (e,x:float) = List.map (fun (p,q) -> (p+x, q+x)) e


let rec merge = function
    | ([], qs)               -> qs
    | (ps, [])               -> ps
    | ((p,_)::ps, (_,q)::qs) -> (p,q) :: merge (ps,qs)

let mergelist es = List.foldBack (fun e acc -> (merge (e,acc))) es []

let rec fit = function
    | ((_,p)::ps, ((q,_)::qs)) -> max (fit (ps,qs)) (p - q + 1.0)
    | _                        -> 0.0

let fitlistl es = let rec fitlistl' acc = function
                            | [] -> []
                            | e::es -> let x = fit (acc,e)
                                       x :: fitlistl' (merge (acc, moveextent (e,x))) es
                  fitlistl' [] es

let fitlistr es = let flipextent e = List.map (fun (p,q) -> (-q,-p)) e
                  List.rev (List.map (~-) (fitlistl (List.map flipextent (List.rev es))))
                  

let fitlist es = 
    let mean (x,y) = (x+y)/2.0
    List.map mean (List.zip (fitlistl es) (fitlistr es))

let wordThreshold = 8
let wordScale = float (2 * wordThreshold) 

let design tree = let rec design' (Node(label, subtrees)) = 
                       let (trees, extents) = List.unzip (List.map design' subtrees)
                       let positions = fitlist extents
                       let ptrees = List.map movetree (List.zip trees positions)
                       let pextents = List.map moveextent (List.zip extents positions)
                       let space = if label.ToString().Length <= wordThreshold then 0.0 else (float (label.ToString().Length)) / wordScale
                       let resultextent = (-space, space) :: mergelist pextents
                       let resulttree = Node((label, 0.0), ptrees)
                       (resulttree, resultextent)

                  fst (design' tree)



let openfile filename = new System.IO.StreamWriter(filename + ".ps", true)

let clearfile filename = let sw = new System.IO.StreamWriter(filename + ".ps", false)
                         sw.Write("")
                         sw.Close()

let write (sw : System.IO.StreamWriter) (s : string) = sw.Write(s)

let closefile (sw : System.IO.StreamWriter) = sw.Close()                  




let moveto x y = String.concat " " (seq [x.ToString() ;  y.ToString() ; " moveto"])
let lineto x y = String.concat " " (seq [x.ToString() ; y.ToString() ; " lineto"])
let makeLabel s = String.concat "" (seq ["("; s ; ") dup stringwidth pop 2 div neg 0 rmoveto show"])
let stroke = "stroke\n"
let header = String.concat  "\n" (seq ["%!";
                                       "<</PageSize[1400 1000]/ImagingBBox null>> setpagedevice";
                                       "1 1 scale" ;
                                       "700 999 translate" ;
                                       "newpath" ;
                                       "/Times-Roman findfont 10 scalefont setfont";
                                       ""])
let showpage = "showpage"

let writeLabel height hori label = String.concat "\n" (seq [moveto (hori*40.0) (height-10.0); makeLabel label;""])

let writeNode height hori label bounds = String.concat "\n" (seq [writeLabel height hori label;
                                                                  moveto (hori*40.0) (height-20.0);
                                                                  lineto (hori*40.0) (height-40.0);
                                                                  moveto ((fst bounds) * 40.0) (height-40.0) ;
                                                                  lineto ((snd bounds) * 40.0) (height-40.0);
                                                                  ""])
                                  


let writeChild height hori = String.concat "\n" (seq [moveto (hori*40.0) (height-40.0) ; lineto (hori*40.0) (height-80.0); ""])

let extractHori (Node((_,hori), _)) = hori                             
    
let rec last =
    function
    | [] -> failwith "empty list"
    | [x] -> x
    | x::xs -> last xs

let rec toPS (sw : System.IO.StreamWriter) current_height parent_hori =
    function
    | Node((label,relative_hori),[]) -> write sw (writeLabel current_height (parent_hori + relative_hori) label)
    | Node((label,relative_hori),children) -> let bounds = (parent_hori + relative_hori + extractHori (List.head children), parent_hori + relative_hori + extractHori (last children))
                                              write sw (writeNode current_height (parent_hori + relative_hori) label bounds)
                                              List.iter (fun (Node((_,child_hori),_)) -> write sw (writeChild current_height (parent_hori + relative_hori + child_hori))) children
                                              write sw stroke
                                              List.iter (toPS sw (current_height - 80.0) (parent_hori + relative_hori)) children

let makePostscript tree filename=
    clearfile filename
    let sw = openfile filename
    write sw header
    toPS sw 0.0 0.0 tree
    write sw showpage
    closefile sw

let makeNode name f list = Node(name,List.foldBack (fun x acc -> (f x) :: acc) list [])

let makeNodeList name f =  
    function
    | [] -> []
    | list -> [makeNode name f list]

let unOperators = ["-";"!"] 
let binOperators = ["+";"*"; "="; "&&"; "||"; "-"; "<"; ">";"<>";"<=";">=";"/";"%"]

let rec expToTree =
    function
    | N i -> Node(i.ToString(), [])
    | B b -> Node(b.ToString(), [])
    | Access acc -> Node("Access", [accessToTree acc])
    | Addr acc -> Node("Address", [accessToTree acc])
    | Apply(op,[e]) when List.exists (fun x ->  x = op) unOperators -> Node("Apply", [Node(op,[]); expToTree e])
    | Apply (op,[e1;e2]) when List.exists (fun x -> x = op) binOperators -> Node("Apply", [Node(op,[]); expToTree e1; expToTree e2])
    | Apply (s, exps) -> Node("Apply", Node(s,[]) :: makeNodeList "Expressions" expToTree exps)

and accessToTree =
    function
    | AVar s -> Node("AVar " + s,[])
    | AIndex (acc, exp) -> Node("ArrayAccess", [accessToTree acc; expToTree exp])
    | ADeref exp -> Node("Dereference", [expToTree exp])

and stmToTree =
    function
    | PrintLn exp -> Node("Print", [expToTree exp])
    | Ass (acc,exp) -> Node("Assign", [accessToTree acc; expToTree exp])
    | Return None -> Node("Return",[])
    | Return (Some exp) -> Node("Return", [expToTree exp])
    | Alt gc -> Node("If", gcToTree gc)
    | Do (GC []) -> Node("Skip",[])
    | Do gc -> Node("Do", gcToTree gc)
    | Block ([],stms) -> makeNode "Statements" stmToTree stms
    | Block (decs,stms) -> Node("Block", makeNodeList "Declarations" decToTree decs @ makeNodeList "Statements" stmToTree stms)
    | Call (s, exps) -> Node("Call", Node(s,[]) :: makeNodeList "Expressions" expToTree exps)

and decToTree = 
    function
    | VarDec (typ, s) -> Node("VarDec", [Node(s,[]);typToTree typ])
    | FunDec (None, s, decs, stm) -> Node("Procedure", Node(s,[]) :: makeNodeList "Declarations" decToTree decs @ [stmToTree stm])
    | FunDec (Some typ, s, decs, stm) -> Node("Function", Node(s,[]) :: typToTree typ :: makeNodeList "Declarations" decToTree decs @ [stmToTree stm])
    
and typToTree =
    function
    | ITyp -> Node("Int",[])
    | BTyp -> Node("Bool", [])
    | ATyp (typ, None) -> Node("Array", [typToTree typ])
    | ATyp (typ, Some i) -> Node("Array", [Node(i.ToString(),[]); typToTree typ])
    | PTyp typ -> Node("Pointer", [typToTree typ])
    | FTyp (typs, None) -> Node("Procedure", makeNodeList "Parameters" typToTree typs)
    | FTyp (typs, Some typ) -> Node("Function", typToTree typ :: makeNodeList "Parameters" typToTree typs)

and gcToTree = 
    function
    | GC [(exp,stms)] -> expToTree exp :: makeNodeList "Statements" stmToTree stms
    | GC list -> List.mapi (fun i (exp, stms) -> Node("Case " + (i+1).ToString(), expToTree exp :: makeNodeList "Statements" stmToTree stms)) list

and programToTree (P (decs,stms)) = Node("Program", makeNodeList "Declarations" decToTree decs @ makeNodeList "Statements" stmToTree stms)

let rec countNodes =
    function
    | Node(s,[]) -> 1
    | Node(s,list) -> 1 + List.foldBack (fun x acc -> (countNodes x)+acc) list 0

let nodeListCount f =  
    function
    | [] -> 0
    | list -> List.foldBack (fun x acc -> (f x) + acc) list 0

let rec expCount =
    function
    | N i -> 1
    | B b -> 1
    | Access acc -> 1 + (accessCount acc)
    | Addr acc -> 1 + (accessCount acc)
    | Apply(op,[e]) when List.exists (fun x ->  x = op) unOperators -> 1 + (expCount e)
    | Apply (op,[e1;e2]) when List.exists (fun x -> x = op) binOperators -> 1 + (expCount e1) + (expCount e2)
    | Apply (s, exps) -> 1 + (nodeListCount expCount exps)

and accessCount =
    function
    | AVar s -> 1
    | AIndex (acc, exp) -> 1 + (accessCount acc) + (expCount exp)
    | ADeref exp -> 1 + (expCount exp)

and stmCount =
    function
    | PrintLn exp -> 1 + (expCount exp)
    | Ass (acc,exp) -> 1 + (accessCount acc) + (expCount exp)
    | Return None -> 1
    | Return (Some exp) -> 1 + (expCount exp)
    | Alt gc -> 1 + (gcCount gc)
    | Do (GC []) -> 1
    | Do gc -> 1 + (gcCount gc)
    | Block ([],stms) -> 1 + (nodeListCount stmCount stms)
    | Block (decs,stms) -> 1 + (nodeListCount decCount decs) + (nodeListCount stmCount stms)
    | Call (s, exps) -> 1 + (nodeListCount expCount exps)

and decCount = 
    function
    | VarDec (typ, s) -> 1 + (typCount typ)
    | FunDec (None, s, decs, stm) -> 1 + (nodeListCount decCount decs) + (stmCount stm)
    | FunDec (Some typ, s, decs, stm) -> 1 + (typCount typ) + (nodeListCount decCount decs) + (stmCount stm)
    
and typCount =
    function
    | ITyp -> 1
    | BTyp -> 1
    | ATyp (typ, None) -> 1 + (typCount typ)
    | ATyp (typ, Some i) -> 1 + (typCount typ)
    | PTyp typ -> 1 + (typCount typ)
    | FTyp (typs, None) -> 1 + (nodeListCount typCount typs)
    | FTyp (typs, Some typ) -> 1 + (nodeListCount typCount typs) + (typCount typ)

and gcCount = 
    function
    | GC list -> 1 + (List.fold (fun acc (exp, stms) -> (expCount exp) + (nodeListCount stmCount stms) + acc) 0 list)

and programCount (P (decs,stms)) = 1 + (nodeListCount decCount decs) + (nodeListCount stmCount stms)

let duration f =
    let timer = new System.Diagnostics.Stopwatch()
    timer.Start()
    let returnValue = f()
    printfn "Time in ms: %i" timer.ElapsedMilliseconds
    returnValue

let rec makeTree =
    function
    | 1 -> Node("Label",[])
    | depth -> Node("Label", [makeTree (depth-1); makeTree (depth-1)])

