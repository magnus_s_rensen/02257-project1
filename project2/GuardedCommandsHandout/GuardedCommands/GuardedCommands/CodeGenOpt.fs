﻿namespace GuardedCommands.Backend
// Michael R. Hansen 05-01-2016

open System
open Machine

open GuardedCommands.Frontend.AST

module CodeGenerationOpt =

   type Var = 
     | GloVar of int                   (* absolute address in stack           *)
     | LocVar of int                   (* address relative to bottom of frame *)

(* The variable environment keeps track of global and local variables, and 
   keeps track of next available offset for local variables *)

   type varEnv = Map<string, Var * Typ> * int

(* The function environment maps function name to label and parameter decs *)

   type ParamDecs = (Typ * string) list
   type funEnv = Map<string, label * Typ option * ParamDecs>


(* Directly copied from Peter Sestoft   START  
   Code-generating functions that perform local optimizations *)

   let rec addINCSP m1 C : instr list =
       match C with
       | INCSP m2            :: C1 -> addINCSP (m1+m2) C1
       | RET m2              :: C1 -> RET (m2-m1) :: C1
       | Label lab :: RET m2 :: _  -> RET (m2-m1) :: C
       | _                         -> if m1=0 then C else INCSP m1 :: C

   let addLabel C : label * instr list =          (* Conditional jump to C *)
       match C with
       | Label lab :: _ -> (lab, C)
       | GOTO lab :: _  -> (lab, C)
       | _              -> let lab = newLabel() 
                           (lab, Label lab :: C)

   let makeJump C : instr * instr list =          (* Unconditional jump to C *)
       match C with
       | RET m              :: _ -> (RET m, C)
       | Label lab :: RET m :: _ -> (RET m, C)
       | Label lab          :: _ -> (GOTO lab, C)
       | GOTO lab           :: _ -> (GOTO lab, C)
       | _                       -> let lab = newLabel() 
                                    (GOTO lab, Label lab :: C)

   let makeCall m lab C : instr list =
       match C with
       | RET n            :: C1 -> TCALL(m, n, lab) :: C1
       | Label _ :: RET n :: _  -> TCALL(m, n, lab) :: C
       | _                      -> CALL(m, lab) :: C

   let rec deadcode C =
       match C with
       | []              -> []
       | Label lab :: _  -> C
       | _         :: C1 -> deadcode C1

   let addNOT C =
       match C with
       | NOT        :: C1 -> C1
       | IFZERO lab :: C1 -> IFNZRO lab :: C1 
       | IFNZRO lab :: C1 -> IFZERO lab :: C1 
       | _                -> NOT :: C

   let addJump jump C =                    (* jump is GOTO or RET *)
       let C1 = deadcode C
       match (jump, C) with
       | (GOTO lab1, Label lab2 :: _) -> if lab1=lab2 then C1 
                                         else GOTO lab1 :: C1
       | _                            -> jump :: C
    
   let addGOTO lab C = addJump (GOTO lab) C

   let bToi b = if b then 1 else 0

   let rec addCST i C =
       match (i, C) with
       | (0, ADD        :: C1) -> C1
       | (0, SUB        :: C1) -> C1
       | (0, NOT        :: C1) -> addCST 1 C1
       | (_, NOT        :: C1) -> addCST 0 C1
       | (1, MUL        :: C1) -> C1
       | (1, DIV        :: C1) -> C1
       | (0, EQ         :: C1) -> addNOT C1
       | (_, INCSP m    :: C1) -> if m < 0 then addINCSP (m+1) C1
                                  else CSTI i :: C
       | (0, IFZERO lab :: C1) -> addGOTO lab C1
       | (_, IFZERO lab :: C1) -> C1
       | (0, IFNZRO lab :: C1) -> C1
       | (_, IFNZRO lab :: C1) -> addGOTO lab C1

       // int math reduction
       | (n1, CSTI n2 :: ADD :: C1) -> addCST (n1 + n2) C1
       | (n1, CSTI n2 :: SUB :: C1) -> addCST (n1 - n2) C1
       | (n1, CSTI n2 :: MUL :: C1) -> addCST (n1 * n2) C1
       | (n1, CSTI n2 :: DIV :: C1) -> addCST (n1 / n2) C1
       | (n1, CSTI n2 :: MOD :: C1) -> addCST (n1 % n2) C1

       // comparison reduction
       | (n1, CSTI n2 :: LT :: NOT :: C1)         -> addCST (bToi (n1 >= n2)) C1
       | (n1, CSTI n2 :: LT :: C1)                -> addCST (bToi (n1 < n2)) C1
       | (n1, CSTI n2 :: SWAP :: LT :: NOT :: C1) -> addCST (bToi (n1 <= n2)) C1
       | (n1, CSTI n2 :: SWAP :: LT :: C1)        -> addCST (bToi (n1 > n2)) C1
       | (n1, CSTI n2 :: EQ :: NOT :: C1)         -> addCST (bToi (n1 <> n2)) C1
       | (n1, CSTI n2 :: EQ :: C1)                -> addCST (bToi (n1 = n2)) C1

   
       | _                     -> CSTI i :: C

   let rec collect f k = function
       | [] -> k
       | x::xs -> collect f ((f x) :: k) xs

   
            
(* ------------------------------------------------------------------- *)

(* End code directly copied from Peter Sestoft *)
/// CE e vEnv fEnv k gives the code for an expression e on the basis of a variable and a function environment and continuation k
   let rec CE vEnv fEnv k = 
       function
       | N n            -> addCST n k
       | B b            -> addCST (bToi b) k
       | Access acc     -> CA vEnv fEnv (LDI :: k) acc
       | Addr acc       -> CA vEnv fEnv k acc 

       | Apply("-",[e]) -> CE vEnv fEnv (addCST 0 (SWAP:: SUB :: k)) e

       | Apply("!",[e]) -> CE vEnv fEnv (addNOT k) e

       | Apply("&&",[b1;b2]) -> let labend   = newLabel()
                                let labfalse = newLabel()
                                CE vEnv fEnv (IFZERO labfalse :: (CE vEnv fEnv (GOTO labend :: Label labfalse :: addCST 0 (Label labend :: k)) b2)) b1

       | Apply("||",[b1;b2]) -> let labend   = newLabel()
                                let labfalse = newLabel()
                                CE vEnv fEnv (IFNZRO labfalse :: (CE vEnv fEnv (GOTO labend :: Label labfalse :: addCST 1 (Label labend :: k)) b2)) b1

       | Apply(o,[e1;e2])  when List.exists (fun x -> o=x) ["+"; "*"; "=";"-";"<>";"<";">";">=";"<=";"%";"/"]
                          -> let ins = match o with
                                       | "+"  -> ADD::k
                                       | "*"  -> MUL::k
                                       | "="  -> EQ::k
                                       | "-"  -> SUB::k
                                       | "%"  -> MOD::k
                                       | "/"  -> DIV::k
                                       | "<>" -> EQ::addNOT k  
                                       | "<"  -> LT::k 
                                       | ">=" -> LT::addNOT k
                                       | ">"  -> SWAP::LT::k 
                                       | "<=" -> SWAP::LT::addNOT k
                                       | _    -> failwith "CE: this case is not possible"
                             CE vEnv fEnv (CE vEnv fEnv ins e2) e1

       | Apply(f,e)   -> let (lab,_,_) = Map.find f fEnv
                         CEs vEnv fEnv (CALL(List.length e, lab) :: k) e
       | _            -> failwith "CE: not supported yet"
       
   and CEs vEnv fEnv k = 
      function
      | []     -> k
      | e::es' -> CE vEnv fEnv (CEs vEnv fEnv k es') e


/// CA acc vEnv fEnv k gives the code for an access acc on the basis of a variable and a function environment and continuation k
   and CA vEnv fEnv k = 
      function
      | AVar x         -> match Map.find x (fst vEnv) with
                          | (GloVar addr,_) -> addCST addr k
                          | (LocVar addr,_) -> GETBP :: (addCST addr (ADD :: k))
      | AIndex(acc, e) -> CA vEnv fEnv (LDI :: (CE vEnv fEnv (ADD :: k) e)) acc 
      | ADeref e       -> CE vEnv fEnv k e

   
(* Bind declared variable in env *)  
   let allocate kind (typ, x) vEnv  =
       let (env, fdepth) = vEnv 
       match typ with
       | ATyp (ATyp _, _) -> failwith "allocate: array of arrays not permitted"
       | ATyp (t, Some i) -> ((Map.add x (kind (fdepth+i), typ) env), fdepth+i+1)
       | _                -> (Map.add x (kind fdepth, typ) env, fdepth+1)

   let allocateList kind vEnv list = List.fold (fun env dec -> match dec with
                                                               | VarDec(typ,s) -> allocate kind (typ,s) env
                                                               | _             -> failwith "parameter is not a variable"
                                               ) vEnv list
(* Generate clean up code for the given type of variable *)
   let genCleanUp k = function
       | ATyp (ATyp _, _) -> failwith "genCleanUp: array of arrays not permitted"
       | ATyp (t, Some i) -> addINCSP i (GETSP :: addCST (i-1) (SUB :: k))
       | _                -> addINCSP 1 k 
   let rec genCleanUpList k = function
       | [] -> k
       | VarDec(typ,_)::rest -> genCleanUp (genCleanUpList k rest) typ
       | _ -> failwith "parameter is not a variable"

   

                      
/// CS s vEnv fEnv k gives the code for a statement s on the basis of a variable and a function environment and continuation k                            
   let rec CS vEnv fEnv isGlobal k = 
       function
       | PrintLn e         -> CE vEnv fEnv (PRINTI:: addINCSP -1 k) e
                           
       | Ass(acc,e)        -> CA vEnv fEnv (CE vEnv fEnv (STI:: addINCSP -1 k) e) acc
                           
       | Block([],stms)    -> CSs vEnv fEnv isGlobal k stms
       | Block(local,stms) -> let newvEnv = allocateList (if isGlobal then GloVar else LocVar) vEnv local
                              genCleanUpList (CSs newvEnv fEnv isGlobal (addINCSP (snd vEnv - snd newvEnv) k) stms) local
       | Alt(GC(list))     -> let labend = newLabel()
                              genAltDoCode vEnv fEnv isGlobal (Label labend :: k) labend list

       | Do(GC([]))        -> k                         
       | Do(GC(list))      -> let labstart = newLabel()
                              Label labstart :: genAltDoCode vEnv fEnv isGlobal k labstart list

       | Return e          -> match e with
                              | Some x -> CE vEnv fEnv ((RET (snd vEnv))::k) x   
                              | None   -> [RET (snd vEnv - 1)]
                           
       | Call(f,e)         -> let (lab,_,_) = Map.find f fEnv
                              CEs vEnv fEnv (CALL(List.length e, lab) :: addINCSP -1 k) e
                                                          
   and CSs vEnv fEnv isGlobal k = 
        function
        | []         -> k
        | stm::stms' -> CS vEnv fEnv isGlobal (CSs vEnv fEnv isGlobal k stms') stm
   and genAltDoCode vEnv fEnv isGlobal k terminationLabel =
        function
        | []             -> k
        | (e,stms)::rest -> let labfalse = newLabel()
                            CE vEnv fEnv ((IFZERO labfalse) :: (CSs vEnv fEnv isGlobal ((GOTO terminationLabel) :: ((Label labfalse) :: genAltDoCode vEnv fEnv isGlobal k terminationLabel rest)) stms)) e 

(* ------------------------------------------------------------------- *)

(* Build environments for global variables and functions *)

   let makeGlobalEnvs decs = 
       let rec addv decs vEnv fEnv = 
           match decs with 
           | []         -> (vEnv, fEnv, [])
           | dec::decr  -> 
                    match dec with
                    | VarDec (typ, var)           -> let vEnv1 = allocate GloVar (typ, var) vEnv
                                                     let (vEnv2, fEnv2, code) = addv decr vEnv1 fEnv
                                                     (vEnv2, fEnv2, genCleanUp code typ)
                    | FunDec (tyOpt, f, xs, body) -> addv decr vEnv (Map.add f (newLabel(), tyOpt, xs) fEnv)
       addv decs (Map.empty, 0) Map.empty

(* Compile a complete micro-C program: globals, call to main, functions *)

   let CP (P(decs,stms)) = 
       let _ = resetLabels ()
       let ((gvM,_) as gvEnv, fEnv, initCode) = makeGlobalEnvs decs

       let cFun (tyOpt, f, xs, body) =
           let (lab, _, decs') = Map.find f fEnv
           let lvEnv = List.fold (fun (env,depth) dec -> match dec with
                                                         | VarDec(typ, s) -> ((Map.add s (LocVar depth, typ) env), depth + 1)
                                                         | _              -> failwith "parameter is not a variable"
                                 ) (fst gvEnv, 0) decs' 
           Label lab :: CS lvEnv fEnv false [RET (List.length decs'-1)] body
       let functions = 
           List.choose (function 
                            | FunDec(t, f, decs, body) -> Some (cFun (t, f, decs, body))
                            | VarDec _                 -> None
                       ) decs
       
       initCode @ CSs gvEnv fEnv true (STOP :: List.concat functions) stms    