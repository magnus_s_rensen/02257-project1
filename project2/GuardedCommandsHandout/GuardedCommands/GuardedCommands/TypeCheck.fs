﻿namespace GuardedCommands.Frontend
// Michael R. Hansen 06-01-2016

open System
open Machine
open GuardedCommands.Frontend.AST

module TypeCheck = 


/// tcE gtenv ltenv e gives the type for expression e on the basis of type environments gtenv and ltenv
/// for global and local variables 
   let rec tcE gtenv ltenv = function                            
         | N _              -> ITyp   
         | B _              -> BTyp   
         | Access acc       -> tcA gtenv ltenv acc  
         
         | Addr acc         -> PTyp (tcA gtenv ltenv acc)
                   
         | Apply(f,[e]) when List.exists (fun x ->  x=f) ["-";"!"]  
                            -> tcMonadic gtenv ltenv f e        
         | Apply(f,[e1;e2]) when List.exists (fun x ->  x=f) ["+";"*"; "="; "&&"; "||"; "-"; "<"; ">";"<>";"<=";">=";"/";"%"]        
                            -> tcDyadic gtenv ltenv f e1 e2   
         | Apply(f,e)     -> tcNaryFunction gtenv ltenv f e

         | _                -> failwith "tcE: not supported yet"

   and tcMonadic gtenv ltenv f e = match (f, tcE gtenv ltenv e) with
                                   | ("-", ITyp) -> ITyp
                                   | ("!", BTyp) -> BTyp
                                   | _           -> failwith "illegal/illtyped monadic expression" 
   
   and tcDyadic gtenv ltenv f e1 e2 = match (f, tcE gtenv ltenv e1, tcE gtenv ltenv e2) with
                                      | (o, ITyp, ITyp) when List.exists (fun x ->  x=o) ["+";"*";"-";"/";"%"]  -> ITyp
                                      | (o, ITyp, ITyp) when List.exists (fun x ->  x=o) ["=";"<";">";"<>";"<=";">="] -> BTyp
                                      | (o, BTyp, BTyp) when List.exists (fun x ->  x=o) ["&&";"=";"<>";"||"]     -> BTyp 
                                      | _                      -> failwith("illegal/illtyped dyadic expression: " + f)

   and tcNaryFunction gtenv ltenv f es = match Map.tryFind f gtenv with
                                         | Some (FTyp(list, Some t)) -> let typList = List.map (tcE gtenv ltenv) es
                                                                        if typList <> list then failwith ("incorrect parameters in call for function: " + f)
                                                                        t
                                         | _                       -> failwith "undefined function"
                                         
 
   and tcNaryProcedure gtenv ltenv f es = match Map.tryFind f gtenv with
                                          | Some (FTyp(list, None)) -> let typList = List.map (tcE gtenv ltenv) es
                                                                       if typList <> list then failwith ("incorrect parameters in call for procedure: " + f)
                                          | _                       -> failwith "undefined procedure"

      

/// tcA gtenv ltenv e gives the type for access acc on the basis of type environments gtenv and ltenv
/// for global and local variables 
   and tcA gtenv ltenv = 
         function 
         | AVar x         -> match Map.tryFind x ltenv with
                             | None   -> match Map.tryFind x gtenv with
                                         | None   -> failwith ("no declaration for : " + x)
                                         | Some t -> tcAhelper t
                             | Some t -> tcAhelper t            
         | AIndex(acc, e) -> let eTyp = tcE gtenv ltenv e
                             if eTyp <> ITyp then failwith "Array index must be an integer"
                             let aTyp = tcA gtenv ltenv acc
                             match aTyp with
                             | ATyp (t,_) -> t
                             | _ -> aTyp
         | ADeref e       -> tcE gtenv ltenv e
   and tcAhelper t = 
        match t with 
        | ATyp (t,Some i) -> ATyp (t,None)
        | PTyp t          -> t
        | t               -> t 

/// tcS gtenv ltenv retOpt s checks the well-typeness of a statement s on the basis of type environments gtenv and ltenv
/// for global and local variables and the possible type of return expressions 
   and tcS gtenv ltenv = function                           
                         | PrintLn e -> ignore(tcE gtenv ltenv e)
                         | Ass(acc,e) -> if tcA gtenv ltenv acc = tcE gtenv ltenv e 
                                         then ()
                                         else failwith "illtyped assignment" 
                         | Block([],stms) -> List.iter (tcS gtenv ltenv) stms
                         | Block(decs,stms)       -> let newltenv = tcGDecs ltenv decs
                                                     List.iter (tcS gtenv newltenv) stms
                         | Alt(GC(list)) 
                         | Do(GC(list))  -> List.iter (fun (e,stms) ->
                                                        if BTyp <> tcE gtenv ltenv e then failwith "not a bool in left hand side"
                                                        List.iter (tcS gtenv ltenv) stms
                                                        ) list
                         | Return e       -> match e with
                                             | Some x -> ignore(tcE gtenv ltenv x)
                                             | None   -> ()

                         | Call(s,e)     -> tcNaryProcedure gtenv ltenv s e
   
   and tcF gtenv = function
                    | FunDec(topt, f, decs, stm) ->  let newltenv = tcGDecs Map.empty decs
                                                     ignore(tcS gtenv newltenv stm)
                                                     ignore(returnTypeCheck gtenv newltenv topt stm)
                                                     let noDuplicates = Seq.distinctBy (fun e -> match e with 
                                                                                                 | VarDec(_,x) -> x
                                                                                                 | FunDec(_,x,_,_) -> x) (Seq.ofList decs)
                                                     if Seq.length noDuplicates <> List.length decs then failwith ("duplicate name in parameters for function: " + f) 
                    | _ -> ()

   and tcGDec gtenv = function  
                      | VarDec(t,s)               -> Map.add s t gtenv
                      | FunDec(topt,f, decs, stm) -> let typList = List.map (fun d -> match d with
                                                                                      | VarDec(t,_) -> t
                                                                                      | FunDec(t,_,_,_) -> match t with
                                                                                                           | Some t -> t
                                                                                                           | None   -> failwith "procedure type list unsupported"
                                                                             ) decs
                                                     let newgtenv = Map.add f (FTyp(typList, topt)) gtenv
                                                     newgtenv
                                                     
   and returnTypeCheck gtenv ltenv ftype = function     
                           | Return e -> match (e,ftype) with
                                         | (Some t1, Some t2) -> ignore((tcE gtenv ltenv t1) = t2)
                                         | (None, None)       -> ()
                                         | _                  -> failwith "incorrect return type"       
                           | Alt(GC(list)) | Do(GC(list)) -> List.iter (fun (e,stms) -> List.iter (returnTypeCheck gtenv ltenv ftype) stms) list      
                           | Block([], stms) -> List.iter (returnTypeCheck gtenv ltenv ftype) stms    
                           | Block(decs, stms) -> let newltenv = tcGDecs ltenv decs
                                                  List.iter (returnTypeCheck gtenv newltenv ftype) stms
                           | _ -> ()
   and tcGDecs gtenv = function
                       | dec::decs -> tcGDecs (tcGDec gtenv dec) decs
                       | _         -> gtenv


/// tcP prog checks the well-typeness of a program prog
   and tcP(P(decs, stms)) = let gtenv = tcGDecs Map.empty decs
                            List.iter (tcF gtenv) decs
                            List.iter (tcS gtenv Map.empty) stms


  
