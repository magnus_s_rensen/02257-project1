﻿// Michael R. Hansen 05-01-2016

// You must revise 4 pathes occurring in this file 
// The first three are:
#r @"bin\Debug\FSharp.PowerPack.dll";;
#r @"bin\Debug\Machine.dll";
#r @"bin\Debug\VirtualMachine.dll";

#load "AST.fs"
#load "Parser.fs"
#load "Lexer.fs"
#load "TypeCheck.fs"
#load "CodeGen.fs"
#load "CodeGenOpt.fs"
#load "Util.fs"
#load "Script.fsx"


open GuardedCommands.Util
open GuardedCommands.Frontend.TypeCheck
open GuardedCommands.Frontend.AST
//open GuardedCommands.Backend.CodeGeneration
open GuardedCommands.Backend.CodeGenerationOpt

open ParserUtil
open CompilerUtil

open Machine
open VirtualMachine

let testTask1() = List.iter exec ["Ex1.gc"; "Ex2.gc";"Ex3.gc"; "Ex4.gc"; "Ex5.gc"; "Ex6.gc"; "Skip.gc"];;
let testTask2() = List.iter exec ["Ex7.gc"; "fact.gc"; "factRec.gc"; "factCBV.gc"];;
let testTask3() = List.iter exec ["A0.gc"; "A1.gc"; "A2.gc"; "A3.gc"];;
let testTask4() = List.iter exec ["A4.gc"; "Swap.gc"; "QuickSortV1.gc"];;
let testTask5() = List.iter exec ["par1.gc"; "factImpPTyp.gc"; "QuickSortV2.gc"; "par2.gc"];;
let testTask1Opt() = List.iter execOpt ["Ex1.gc"; "Ex2.gc";"Ex3.gc"; "Ex4.gc"; "Ex5.gc"; "Ex6.gc"; "Skip.gc"];;
let testTask2Opt() = List.iter execOpt ["Ex7.gc"; "fact.gc"; "factRec.gc"; "factCBV.gc"];;
let testTask3Opt() = List.iter execOpt ["A0.gc"; "A1.gc"; "A2.gc"; "A3.gc"];;
let testTask4Opt() = List.iter execOpt ["A4.gc"; "Swap.gc"; "QuickSortV1.gc"];;
let testTask5Opt() = List.iter execOpt ["par1.gc"; "factImpPTyp.gc"; "par2.gc"; "QuickSortV2.gc";];;


//testTask1()
//testTask2()
//testTask3()
//testTask4()
//testTask5()
//
//testTask1Opt()
//testTask2Opt()
//testTask3Opt()
//testTask4Opt()
//testTask5Opt()

//let tree = parseFromFile "bla.gc"
//CP tree
execOpt "bla.gc"
