﻿namespace GuardedCommands.Backend
// Michael R. Hansen 05-01-2016
// This file is obtained by an adaption of the file MicroC/Comp.fs by Peter Sestoft
open System
open Machine

open GuardedCommands.Frontend.AST
module CodeGeneration =


(* A global variable has an absolute address, a local one has an offset: *)
   type Var = 
     | GloVar of int                   (* absolute address in stack           *)
     | LocVar of int                   (* address relative to bottom of frame *)

(* The variable environment keeps track of global and local variables, and 
   keeps track of next available offset for local variables *)

   type varEnv = Map<string, Var*Typ> * int

(* The function environment maps function name to label and parameter decs *)

   type ParamDecs = (Typ * string) list
   type funEnv = Map<string, label * Typ option * ParamDecs>

/// CE vEnv fEnv e gives the code for an expression e on the basis of a variable and a function environment
   let rec CE vEnv fEnv = 
       function
       | N n          -> [CSTI n]
       | B b          -> [CSTI (if b then 1 else 0)]
       | Access acc   -> CA vEnv fEnv acc @ [LDI]
       
       | Addr acc     -> CA vEnv fEnv acc 

       | Apply("-", [e]) -> CE vEnv fEnv e @  [CSTI 0; SWAP; SUB]
       | Apply("!",[e]) -> CE vEnv fEnv e @ [NOT]

       | Apply("&&",[b1;b2]) -> let labend   = newLabel()
                                let labfalse = newLabel()
                                CE vEnv fEnv b1 @ [IFZERO labfalse] @ CE vEnv fEnv b2
                                @ [GOTO labend; Label labfalse; CSTI 0; Label labend]
       | Apply("||",[b1;b2]) -> let labend   = newLabel()
                                let labtrue = newLabel()
                                CE vEnv fEnv b1 @ [IFNZRO labtrue] @ CE vEnv fEnv b2
                                @ [GOTO labend; Label labtrue; CSTI 1; Label labend]

       | Apply(o,[e1;e2]) when List.exists (fun x -> o=x) ["+"; "*"; "=";"-";"<>";"<";">";">=";"<=";"/";"%"]
                             -> let ins = match o with
                                          | "+"  -> [ADD]
                                          | "-"  -> [SUB]
                                          | "*"  -> [MUL]
                                          | "/"  -> [DIV]
                                          | "%"  -> [MOD]
                                          | "="  -> [EQ] 
                                          | "<>" -> [EQ;NOT]  
                                          | "<"  -> [LT] 
                                          | ">=" -> [LT;NOT]
                                          | ">"  -> [SWAP;LT] 
                                          | "<=" -> [SWAP;LT;NOT]
                                          | _    -> failwith "CE: this case is not possible"
                                CE vEnv fEnv e1 @ CE vEnv fEnv e2 @ ins
       | Apply(f,e)   -> let (lab,_,_) = Map.find f fEnv
                         List.collect (CE vEnv fEnv) e @ [CALL(List.length e, lab)] 

       | _            -> failwith "CE: not supported yet"


/// CA vEnv fEnv acc gives the code for an access acc on the basis of a variable and a function environment
   and CA vEnv fEnv = function | AVar x         -> match Map.find x (fst vEnv) with
                                                   | (GloVar addr,_) -> [CSTI addr]
                                                   | (LocVar addr,_) -> if Map.isEmpty fEnv then [CSTI addr] else [GETBP] @ [CSTI addr] @ [ADD]
                               | AIndex(acc, e) -> CA vEnv fEnv acc @ [LDI] @ CE vEnv fEnv e @ [ADD]
                               | ADeref e       -> CE vEnv fEnv e

  

(* Bind declared variable in env and generate code to allocate it: *)   
   let allocate (kind : int -> Var) (typ, x) (vEnv : varEnv)  =
    let (env, fdepth) = vEnv 
    match typ with
    | ATyp (ATyp _, _) -> raise (Failure "allocate: array of arrays not permitted")
    | ATyp (t, Some i) -> let newEnv = ((Map.add x (kind (fdepth+i), typ) env), fdepth+i+1) //borrowed from microC
                          let code = [INCSP i; GETSP; CSTI (i-1); SUB]
                          (newEnv, code)

    | _ -> let newEnv = (Map.add x (kind fdepth, typ) env, fdepth+1)
           let code = [INCSP 1]
           (newEnv, code)

                      
/// CS vEnv fEnv s gives the code for a statement s on the basis of a variable and a function environment                          
   let rec CS vEnv fEnv isGlobal = function
       | PrintLn e        -> CE vEnv fEnv e @ [PRINTI; INCSP -1] 

       | Ass(acc,e)       -> CA vEnv fEnv acc @ CE vEnv fEnv e @ [STI; INCSP -1]

       | Block([],stms)    ->   CSs vEnv fEnv isGlobal stms

       | Block(local,stms) -> let (newvEnv, instlist) = List.fold (fun (env,instlist) (VarDec(typ,s)) -> let (newEnv,code) = allocate (if isGlobal then GloVar else LocVar) (typ,s) env
                                                                                                         (newEnv, instlist @ code)
                                                                  ) (vEnv, []) local
                              instlist @ CSs newvEnv fEnv isGlobal stms @ [INCSP (snd vEnv - snd newvEnv)]

       | Alt(GC(list)) ->   let labend = newLabel()
                            (List.collect (fun (e,stms) -> 
                                         let labfalse = newLabel()
                                         CE vEnv fEnv e @ [IFZERO labfalse] @ CSs vEnv fEnv isGlobal stms @ [GOTO labend] @ [Label labfalse]
                                         ) list) @ [Label labend] 

       | Do(GC([]))   ->   []
                                         
       | Do(GC(list)) ->   let labstart = newLabel()
                           [Label labstart] @ List.collect (fun (e,stms) -> 
                                         let labfalse = newLabel()
                                         CE vEnv fEnv e @ [IFZERO labfalse] @ CSs vEnv fEnv isGlobal stms @ [GOTO labstart] @ [Label labfalse]
                                         ) list 

       | Return e    ->     match e with
                            | Some x -> CE vEnv fEnv x @ [RET (snd vEnv)]
                            | None   -> [RET (snd vEnv - 1)]

       | Call(f,e)   ->    let (lab,_,_) = Map.find f fEnv
                           List.collect (CE vEnv fEnv) e @ [CALL(List.length e, lab)] @ [INCSP -1]

   and CSs vEnv fEnv isGlobal stms = List.collect (CS vEnv fEnv isGlobal) stms 


(* ------------------------------------------------------------------- *)

(* Build environments for global variables and functions *)

   let makeGlobalEnvs decs = 
       let rec addv decs vEnv fEnv = 
           match decs with 
           | []         -> (vEnv, fEnv, [])
           | dec::decr  -> 
             match dec with
             | VarDec (typ, var) -> let (vEnv1, code1) = allocate GloVar (typ, var) vEnv
                                    let (vEnv2, fEnv2, code2) = addv decr vEnv1 fEnv
                                    (vEnv2, fEnv2, code1 @ code2)
             | FunDec (tyOpt, f, xs, body) -> addv decr vEnv (Map.add f (newLabel(), tyOpt, xs) fEnv)
                                       
       addv decs (Map.empty, 0) Map.empty




/// CP prog gives the code for a program prog
   let CP (P(decs,stms)) = 
       let _ = resetLabels ()
       let ((gvM,_) as gvEnv, fEnv, initCode) = makeGlobalEnvs decs

       let cFun (tyOpt, f, xs, body) =
           let (lab, _, decs') = Map.find f fEnv
           let lvEnv = List.fold (fun (env,depth) (VarDec(typ,x)) -> ((Map.add x (LocVar depth, typ) env), depth + 1)) (fst gvEnv, 0) decs' 
           let code = CS lvEnv fEnv false body
           [Label lab] @ code @ [RET (List.length decs'-1)]
       let functions = 
           List.choose (function 
                            | FunDec(t, f, decs, body) -> Some (cFun (t, f, decs, body))
                            | VarDec _ -> None
                       ) decs




       initCode
       @ CSs gvEnv fEnv true stms 
       @ [STOP]
       @ List.concat functions     



