﻿module Utility

let nimsum heaps = Array.fold (fun sum elem -> sum ^^^ elem) 0 heaps //Calculate m = a0 xorb a1 xorb...
let isReduced heap ns = (heap ^^^ ns) < heap
let gameover heaps = Array.forall (fun elem -> elem = 0) heaps
let rec generatemap() = let rnd = System.Random()
                        let map = Array.init (rnd.Next(3,10)) (fun _ -> rnd.Next(1,10))
                        if nimsum map = 0
                        then generatemap()
                        else map