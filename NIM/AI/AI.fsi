﻿module Logic

type Move = Optimal | Neutral | Bad
val move : int [] -> int -> int [] * Move
val hint: int[] -> string