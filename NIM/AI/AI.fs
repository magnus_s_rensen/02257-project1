﻿module Logic

open Utility

type Move = Optimal | Neutral | Bad //Move type

// general move calculator
let calculatemove heaps searchFunction = let ns = Utility.nimsum heaps
                                         match Array.tryFindIndex (fun elem -> (searchFunction elem ns)  && ns <> 0) heaps with //Optimal move (case 1 of the strategy)
                                         | Some i -> Array.set heaps i (heaps.[i] ^^^ ns)
                                                     (heaps, Optimal)
                                         | None   -> match Array.tryFindIndex (fun elem -> elem = (Array.max heaps)) heaps with //No good move (case 2 of the strategy)
                                                     | Some i -> Array.set heaps i (heaps.[i] - 1)
                                                                 (heaps, Neutral)
                                                     | None   -> failwith "all heaps are empty"
   
let hint heaps = let ns = Utility.nimsum heaps
                 match Array.tryFindIndex (fun elem -> (isReduced elem ns)  && ns <> 0) heaps with //Optimal move (case 1 of the strategy)
                 | Some i -> "Optimal move: Change heap " + (i + 1).ToString() + " to " + (heaps.[i] ^^^ ns).ToString()
                 | None   -> match Array.tryFindIndex (fun elem -> elem = (Array.max heaps)) heaps with //No good move (case 2 of the strategy)
                             | Some i -> "No good move. Change heap " + (i + 1).ToString() + " to " + (heaps.[i] - 1).ToString()
                             | None   -> failwith "all heaps are empty"

//Make a random move. Choose a random heap among non-zero heaps and remove a random amount from it (at most original value)   
let randomMove heaps = let indexedHeaps = Array.mapi (fun i x -> (i,x)) heaps
                       let filteredHeaps = Array.filter (fun (_,x) -> x <> 0) indexedHeaps
                       let rnd = System.Random()
                       let filteredIndex = rnd.Next(0,filteredHeaps.Length)
                       let index = fst filteredHeaps.[filteredIndex]
                       let value = heaps.[index]
                       heaps.[index] <- rnd.Next(0,value)
                       heaps


// move utility functions
let goodmove heaps = calculatemove heaps Utility.isReduced
let badmove heaps = calculatemove heaps (fun _ _ -> false)

// move function with difficulty, higher number = higher difficulty
let move heaps diff = if diff < 1 || diff > 100
                      then failwith "wrong difficulty, make it a number between 1 and 100"
                      let rnd = System.Random()
                      if rnd.Next(1,99) < diff
                      then goodmove heaps
                      else if rnd.Next(0,2) = 0
                           then (randomMove heaps, Bad) //Make a random move
                           else (fst (badmove heaps), Bad) //Intentionally use case 2 of the strategy, no matter the state of the game
