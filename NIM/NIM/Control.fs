﻿module Control

open System 
open System.Net 
open System.Threading 
open System.Windows.Forms 
open System.Drawing 
open System.Text.RegularExpressions
open Utility
open Logic
open EventQueue
open GUI


type Message =
   Reset | Move | Get | Web of string | Error | Cancel | Cancelled | Hint | Undo

//Urls to load from
let urls = [|"https://www.reddit.com/r/AskReddit/new/" ; 
             "https://www.reddit.com/r/funny/new/" ; 
             "https://www.reddit.com/r/pics/new/" ; 
             "http://www.dr.dk/"|]

//Taunts
let naughtyTaunts = [|"Beat that!" ; "I like this move." ; "Tihi :)" ; "Go to bed, kid." ; "You're doomed, DOOOOMED!"|]
let neutralTaunts = [|"That'll do." ; "I'll try that." ; "No good move here."|]
let insecureTaunts = [|"I'm not sure that was good." ; "Did I do alright?" ; "I need help." ; "I want my mommy."|]

//Inspired by http://stackoverflow.com/a/14126074
//Used for enhancing the user experience! (make the AI "think")
let Delay ms action = 
    let timer = new Timer()
    timer.Interval <- ms
    timer.Tick.Add (fun _ -> action()
                             timer.Stop())
    timer.Start()
let DEFAULT_AI_DELAY = 1000

let ev = AsyncEventQueue()
//Initial state
let rec ready heaps = 
  async {
         GUI.enableButton GUI.Cancel false
         GUI.enableButton GUI.Hint true
         GUI.enableButton GUI.Undo true
         let! msg = ev.Receive()
         match msg with
         | Reset    -> return! reset()
         | Move     -> return! move heaps 
         | Get      -> return! loading()
         | Hint      -> ignore (GUI.displayHint (Logic.hint heaps)) ;return! ready heaps
         | Undo      -> GUI.setBoxesEqualLabels()
                        GUI.toggleAllBoxes true
                        return! ready heaps
         | _         -> failwith("ready: unexpected message")}
//Create a new game
and reset() =
    async {
        let heaps = Utility.generatemap()
        GUI.resetGameBoard heaps 
        GUI.toggleAllBoxes true
        GUI.setStatusBox "Make a move"
        GUI.enableButton GUI.Move false
        GUI.enableButton GUI.Reset true
        return! ready heaps 
    }
//Make a move
and move (heaps : int []) =
    async {
        let valuesBefore = GUI.getNimValues() //Get the current values of the game board
        GUI.setNimValues valuesBefore
        if Utility.gameover valuesBefore then return! gameover true //Check if player has won
        GUI.setStatusBox "AI thinking..." 
        let (newHeaps, AIstatus) = Logic.move valuesBefore (GUI.getDifficulty()) //Send the heaps to the AI for it to make a move
        GUI.enableButton GUI.Move false
        GUI.enableButton GUI.Undo false
        if Utility.gameover newHeaps //Check if AI has won  
        then GUI.setNimValues newHeaps
             return! gameover false
        else
            Delay DEFAULT_AI_DELAY (fun () -> //Make the AI "think" and respond with an appropriate taunt
                GUI.setNimValues newHeaps
                let rnd = System.Random()
                if not (Utility.gameover newHeaps) then match AIstatus with
                                                        | Logic.Optimal -> let taunt = rnd.Next(0,naughtyTaunts.Length)
                                                                           GUI.setStatusBox (naughtyTaunts.[taunt] + " Your turn...")
                                                        | Logic.Neutral -> let taunt = rnd.Next(0,neutralTaunts.Length)
                                                                           GUI.setStatusBox (neutralTaunts.[taunt] + " Your turn...")
                                                        | Logic.Bad -> let taunt = rnd.Next(0,insecureTaunts.Length)
                                                                       GUI.setStatusBox (insecureTaunts.[taunt] + " Your turn...")
                                                        | _ -> GUI.setStatusBox "Your turn..."
                GUI.toggleAllBoxes true
                )       
        return! ready newHeaps
    }
//Game has ended
and gameover victory =
    async {
        let endMessage = if victory then "You win!" else "You lose!"
        GUI.setStatusBox ("Game over - " + endMessage)
        GUI.enableButton GUI.Move false
        GUI.enableButton GUI.Hint false
        GUI.enableButton GUI.Undo false
        GUI.toggleAllBoxes false
        GUI.centerStatusBox()
        let! msg = ev.Receive()
        match msg with
        | Reset    -> return! reset()
        | Get      -> return! loading()
        | _         -> failwith("ready: unexpected message")
    }
//Create a game by getting the length of a website and using each cipher as a heap
and loading() =
  async {GUI.setStatusBox "Downloading"
         GUI.enableButton GUI.Cancel true
         use ts = new CancellationTokenSource()
         //Pick a random url
         let rnd = System.Random()
         let urlNo = rnd.Next(0,urls.Length)
         let url = urls.[urlNo]
         // start the load
         GUI.enableButton GUI.Reset false
         GUI.enableButton GUI.Move false
         GUI.enableButton GUI.Hint false
         Async.StartWithContinuations
             (async { let webCl = new WebClient()
                      let! html = webCl.AsyncDownloadString(Uri url)
                      return html },
              (fun html -> ev.Post (Web html)),
              (fun _ -> ev.Post Error),
              (fun _ -> ev.Post Cancelled),
              ts.Token)

         let! msg = ev.Receive()
         match msg with
         | Web html ->
             //Convert the length of the website into an array of heaps
             let ans = html.Length.ToString().ToCharArray()
             let newHeaps = Array.map (fun x -> int (Math.Round (Char.GetNumericValue x))) ans
             return! finished newHeaps
         | Error   -> GUI.setStatusBox "Could not get from web. Game reset"
                      return! reset()
         | Cancel  -> ts.Cancel()
                      return! cancelling()
         | _       -> failwith("loading: unexpected message")}

and cancelling() =
  async {GUI.setStatusBox "Cancelling"         
         let! msg = ev.Receive()
         match msg with
         | Cancelled | Error | Web  _ ->
                   return! reset()
         | _    ->  failwith("cancelling: unexpected message")}
//Make game from loading() result
and finished heaps =
    async {
        GUI.resetGameBoard heaps 
        GUI.toggleAllBoxes true
        GUI.setStatusBox "Make a move"
        GUI.enableButton GUI.Move false
        GUI.enableButton GUI.Reset true
        return! ready heaps 
    }

//Launch the game
let startGame() =
    let window = GUI.createGUI
    //Initialize button events
    GUI.setClickEvent GUI.Reset (fun _ -> ev.Post Reset)
    GUI.setClickEvent GUI.Move (fun _ -> ev.Post Move)
    GUI.setClickEvent GUI.Web (fun _ -> ev.Post Get)
    GUI.setClickEvent GUI.Cancel (fun _ -> ev.Post Cancel)
    GUI.setClickEvent GUI.Hint (fun _ -> ev.Post Hint)
    GUI.setClickEvent GUI.Undo (fun _ -> ev.Post Undo)
    Async.StartImmediate (reset())
    Application.Run(window)