﻿module GUI
open System 
open System.Windows.Forms 

type ActionButton = Reset | Move | Cancel | Web | Hint | Undo
val setClickEvent : ActionButton -> (EventArgs -> Unit) -> Unit
val enableButton : ActionButton -> bool -> Unit
val createGUI : Form
val resetGameBoard : int [] -> Unit
val setNimValues : int [] -> Unit
val setStatusBox : string -> Unit
val getNimValues : Unit -> int []
val getDifficulty: Unit -> int
val centerStatusBox: Unit -> Unit
val displayHint : string -> DialogResult
val setBoxesEqualLabels : Unit -> Unit
val toggleAllBoxes : bool -> Unit
