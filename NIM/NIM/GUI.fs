﻿module GUI

open System 
open System.Net 
open System.Threading 
open System.Windows.Forms 
open System.Drawing 
open EventQueue

let window =
  new Form(Text="NIM", Size=Size(700,500))

let label x =
            let temp = new Label()
            do temp.Size <-new System.Drawing.Size(20,20)
            do temp.Location <- new Drawing.Point(x,240) 
            do temp.TextAlign <- ContentAlignment.BottomCenter
            do temp.BackColor <- Color.Blue
            do temp.ForeColor <- Color.Blue
            temp

//Label for helpful messages and taunts
let statusBox =
  new Label(Location=Point(0,5),Size=Size(700,25))
statusBox.Font <- new Font(statusBox.Font.Name,14.0F)
statusBox.TextAlign <- ContentAlignment.MiddleCenter
statusBox.Text <- "Welcome!"

let centerStatusBox() =
    statusBox.Location <- new Point(0,125)
    statusBox.Font <- new Font(statusBox.Font.Name, 30.0F)
    statusBox.Height <- 50

let resetStatusBox() = 
    statusBox.Location <- new Point(0,5)
    statusBox.Font <- new Font(statusBox.Font.Name, 14.0F)
    statusBox.Height <- 25

//Button for creating a new game
let resetButton =
  new Button(Location=Point(50,320),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="RESET")

let moveButton =
  new Button(Location=Point(200,320),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="MOVE")

let webButton =
  new Button(Location=Point(350,320),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="FROM WEB")

let cancelButton =
  new Button(Location=Point(500,320),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="CANCEL")

let HINTLIMIT = 90
let hintButton =
  new Button(Location=Point(500,380),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="HINT")

let undoButton =
  new Button(Location=Point(500,200),MinimumSize=Size(100,50),
              MaximumSize=Size(100,50),Text="UNDO")

//Slider to determine AI difficulty. 100% is the hardest
let DEFAULT_DIFFICULTY = 80
let difficultyDesc = new Label(Location=Point(50,400),Size=Size(100,100))
difficultyDesc.Text <- "Effort level"
difficultyDesc.Font <- new Font(difficultyDesc.Font.Name, 12.0F)
let difficultySlider = new TrackBar(Location=Point(150,400), Width=300, Minimum=1, Maximum=100, TickStyle=TickStyle.None)
difficultySlider.Value <- DEFAULT_DIFFICULTY
let difficultyLabel = new Label(Location=Point(450,400), Size=Size(200,100))
difficultyLabel.Text <- DEFAULT_DIFFICULTY.ToString() + "%"
difficultyLabel.Font <- new Font(difficultyLabel.Font.Name, 12.0F)

let generateXes n =
    [|for i in 0 .. (n-1) -> 50 + i * 50|]

let generateBoxes n = 
    generateXes n
    |> Array.map (fun x -> new TextBox(Location=Point(x,265),Size=Size(25,25)))
     
let generateLabels n = 
    generateXes n
    |> Array.map (fun x -> label x)

let setBoxValues boxes values = 
    Array.iter2 (fun (b : TextBox) v -> b.Text <- v.ToString()) boxes values

let setLabelValues labels values = 
    Array.iter2 (fun (l : Label) v -> l.Text <- v.ToString()
                                      l.Height <- 2 + v*20
                                      l.Location <- new Point(l.Location.X, 240 - l.Height)) labels values



let getBoxValue (box : TextBox) = try int box.Text
                                  with | :? System.FormatException -> -1

                                  
let getLabelValues labels = Array.collect (fun (label : Label) -> [|int label.Text|]) labels

let getBoxValues boxes = Array.collect (fun box -> [|getBoxValue box|]) boxes

//Set the values of labels according to array "values"
let updateLabels labels values = Array.map2 (fun (label : Label) value -> label.Text <- value.ToString()) labels values

//Set the values of textboxes according to array "values"
let updateTextBoxes boxes values = Array.map2 (fun (box : TextBox) value -> box.Text <- value.ToString()) boxes values

let disableTextBoxes boxes = Array.map (fun (box : TextBox) -> box.Enabled <- false) boxes

//Mutable variables for textboxes and labels, allowing for creating new ones
let mutable (textBoxes : TextBox []) = [||]
let mutable (labels : Label []) = [||]

//Disable boxes of index not "index"
let disableNotUsedBoxes index = Array.iteri (fun i elem -> if i <> index then textBoxes.[i].Enabled <- false) textBoxes

let textChangedEvent box _  =  let index = Array.findIndex (fun b -> b = box) textBoxes
                               let nums = getLabelValues labels
                               if box.Text <> "" && getBoxValue box = nums.[index]                                                                                                                        
                               then Array.iter (fun (b : TextBox) -> b.Enabled <- true) textBoxes //If the textbox is changed back to original value, enable all boxes
                               else if getBoxValue box < nums.[index] && getBoxValue box > -1
                                    then disableNotUsedBoxes index;statusBox.Text <- "Press MOVE"; moveButton.Enabled <- true //If a non-original, valid value is entered in a box. 
                                                                                                                              //Disable other boxes and enable move button
                                    else statusBox.Text <- "Input invalid"; moveButton.Enabled <- false
let keypressEvent box (ev : KeyPressEventArgs) = if not (Char.IsNumber ev.KeyChar || Char.IsControl ev.KeyChar)  //Disallow non-numeric characters. Allow control keys (backspace and such)
                                                 then ev.Handled <- true

let addBoxEvents boxes =
    Array.iter (fun (box : TextBox) ->  box.KeyUp.Add (textChangedEvent box)
                                        box.KeyPress.Add (keypressEvent box)) boxes

//Change difficultyLabel according to slider
let setSliderEvent (slider : TrackBar) = 
    difficultySlider.ValueChanged.Add (fun ev -> difficultyLabel.Text <- slider.Value.ToString() + "%"
                                                 if slider.Value >= HINTLIMIT 
                                                 then hintButton.Enabled <- false
                                                 else hintButton.Enabled <- true)

let getDifficulty() = difficultySlider.Value

//Set both label and textbox values
let setAllValues labels boxes values = setBoxValues boxes values
                                       setLabelValues labels values

let setNimValues (values : int []) = setAllValues labels textBoxes values

let getNimValues() = getBoxValues textBoxes

let toggleAllBoxes status = Array.iter (fun (b : TextBox) -> b.Enabled <- status) textBoxes


let enableAllBoxes() = toggleAllBoxes true

let setBoxesEqualLabels() = Array.iter2 (fun (b : TextBox) (l : Label) -> b.Text <- l.Text) textBoxes labels


let disableAllBoxes() = toggleAllBoxes false

let displayHint hint = MessageBox.Show(hint)

let setStatusBox text = statusBox.Text <- text

type ActionButton = Reset | Move | Cancel | Web | Hint | Undo

let enableButton button status = 
    match button with
    | Reset  -> resetButton.Enabled <- status
    | Move   -> moveButton.Enabled <- status
    | Cancel -> cancelButton.Enabled <- status
    | Web    -> webButton.Enabled <- status
    | Hint    -> if difficultySlider.Value >= HINTLIMIT then hintButton.Enabled <- false else hintButton.Enabled <- status
    | Undo    -> undoButton.Enabled <- status

let setClickEvent button event = 
    match button with
    | Reset -> resetButton.Click.Add event
    | Move  -> moveButton.Click.Add event
    | Cancel -> cancelButton.Click.Add event
    | Web  -> webButton.Click.Add event
    | Hint    -> hintButton.Click.Add event
    | Undo    -> undoButton.Click.Add event

//Add elements to GUI
let createGUI = 
    window.Controls.Add resetButton
    window.Controls.Add moveButton
    window.Controls.Add webButton
    window.Controls.Add cancelButton
    window.Controls.Add hintButton
    window.Controls.Add undoButton
    moveButton.Enabled <- false
    window.Controls.Add statusBox
    window.Controls.Add difficultyDesc
    window.Controls.Add difficultySlider
    window.Controls.Add difficultyLabel
    window

//Generate new gameboard according to array
//Removes old boxes and generates new ones
let resetGameBoard (values : int []) = 
    Array.iter (fun box -> window.Controls.Remove box) textBoxes
    Array.iter (fun label -> window.Controls.Remove label) labels
    textBoxes <- generateBoxes values.Length
    labels <- generateLabels values.Length
    Array.iter (fun (box : TextBox) ->  window.Controls.Add box) textBoxes
    Array.iter (fun (label : Label) ->  window.Controls.Add label) labels
    setAllValues labels textBoxes values
    addBoxEvents textBoxes
    setSliderEvent difficultySlider
    resetStatusBox()
